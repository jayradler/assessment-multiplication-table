let i = 0;
let h = 0;
let size = null;

size = prompt("How large do you want the multiplication table?", 10);
while (size > 25 || size <= 0 || isNaN(size) || size == null) {
    size = prompt("What are you crazy? Try a number greater than zero and less than 26", 10);
}
document.write('<span class="cell header">&times;</span>');
while (i <= size) {
    while (h <= size) {
        document.write('<span class="cell header">' + h + "</span>");
        h++;
    }
    document.write("<br>");
    let t = 0;
    document.write('<span class="cell header">' + i + "</span>");
    while (t <= size) {
        document.write('<span class="cell"> ' + i*t + '</span>');
        t++;
    }
i++;
}